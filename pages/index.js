import React from 'react';

import client from './client';

import { ApolloProvider, Query } from 'react-apollo';
import gql from 'graphql-tag';

let generateQuery = price => gql`{
  books(price: ${price}) {
    title
    author
    price
  }
}`;

class Index extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputPrice: 0,
      query: generateQuery(0)
    }

    this.onInputChanged = e => {
      this.setState({
        inputPrice: Number(e.target.value),
      })
    }

    this.onSearchClicked = () => {
      const { inputPrice } = this.state;
      this.setState({
        query: generateQuery(inputPrice)
      })
    }
  }
  render() {
    const { query } = this.state;
    return (
      <ApolloProvider client={client}>
        <div>
          <p>hoge</p>
          <input
            type="text"
            onChange={this.onInputChanged}/>
          <button
            onClick={this.onSearchClicked}>Search</button>
          <Query query={query}>
            {({loading, error, data}) => {
              if (loading) return <p>Loading...</p>;
              if (error) return <p>Error</p>;
              return data.books.map(book => (
                <ul key={book.title}>
                  <li>{book.title}</li>
                  <li>{book.author}</li>
                  <li>{book.price}</li>
                </ul>
              ))}}
          </Query>
        </div>
      </ApolloProvider>
    );
  };
}

export default Index